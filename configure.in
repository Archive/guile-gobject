AC_PREREQ(2.52)
AC_INIT(gobject/gobject.scm)

AM_CONFIG_HEADER(config.h)
AM_INIT_AUTOMAKE(guile-gobject, 0.4.0)

AM_MAINTAINER_MODE

GNOME_COMMON_INIT
GNOME_PLATFORM_GNOME_2(yes, force)

AC_ISC_POSIX
AC_PROG_CC
AC_STDC_HEADERS
AM_PROG_LIBTOOL
AC_PROG_YACC
AC_PATH_PROGS(PATH_TO_XRDB, "xrdb")

# utility conditional
#AM_CONDITIONAL(FALSE, test "x" = "y")

AC_SUBST(CFLAGS)
AC_SUBST(CPPFLAGS)
AC_SUBST(LDFLAGS)

GNOME_COMPILE_WARNINGS(maximum)

# Define GNOME_ENABLE_DEBUG if the --enable-debug switch was given.
GNOME_DEBUG_CHECK

# Don't use AC_PROG_AWK since we need the full pathname.
AC_PATH_PROGS(AWK, mawk gawk nawk awk, )
AC_PATH_PROGS(PERL, perl5 perl)

# define a MAINT-like variable REBUILD which is set if Perl
# and awk are found, so autogenerated sources can be rebuilt
AC_ARG_ENABLE(rebuilds, [  --disable-rebuilds      disable all source autogeneration rules],,enable_rebuilds=yes)
REBUILD=\#
if test "x$enable_rebuilds" = "xyes" && \
     test -n "$PERL" && \
     $PERL -e 'exit !($] >= 5.002)' > /dev/null 2>&1 && \
     test -n "$AWK" ; then
  REBUILD=
fi
AC_SUBST(REBUILD)

#
# Check for Guile
#
AC_MSG_CHECKING(for Guile)
guile-config link > /dev/null || {
    echo "configure: cannot find guile-config; is Guile installed?" 1>&2
    exit 1
}
GUILE_CFLAGS="`guile-config compile`"
GUILE_LIBS="`guile-config link`"
AC_SUBST(GUILE_CFLAGS)
AC_SUBST(GUILE_LIBS)
AC_MSG_RESULT(yes)

#
# Start of pkg-config checks
#
PKG_CHECK_MODULES(GUILE_GNOME_GOBJECT, gobject-2.0 >= 1.3.1)
AC_SUBST(GUILE_GNOME_GOBJECT_CFLAGS)
AC_SUBST(GUILE_GNOME_GOBJECT_LIBS)

PKG_CHECK_MODULES(GUILE_GNOME_GTK, gtk+-2.0 >= 1.3.1)
AC_SUBST(GUILE_GNOME_GTK_CFLAGS)
AC_SUBST(GUILE_GNOME_GTK_LIBS)

PKG_CHECK_MODULES(GUILE_CORBA, gobject-2.0 >= 1.3.1 ORBit-2.0 >= 2.3.90 libIDL-2.0 >= 0.7.0 libbonobo-2.0 >= 1.97.0)
AC_SUBST(GUILE_CORBA_CFLAGS)
AC_SUBST(GUILE_CORBA_LIBS)

PKG_CHECK_MODULES(CORBA_DEMO, gobject-2.0 >= 1.3.1 ORBit-2.0 >= 2.3.90 libbonobo-2.0 >= 1.97.0)
AC_SUBST(CORBA_DEMO_CFLAGS)
AC_SUBST(CORBA_DEMO_LIBS)

# orbit-idl
ORBIT_IDL="`$PKG_CONFIG --variable=orbit_idl ORBit-2.0`"
AC_SUBST(ORBIT_IDL)

# Activation idl files
BONOBO_IDL_DIR="`$PKG_CONFIG --variable=idldir libbonobo-2.0`"
AC_SUBST(BONOBO_IDL_DIR)

AC_OUTPUT([
Makefile
gobject/Makefile
gtk/Makefile
gnome-gwrap/Makefile
corba/Makefile
demos/Makefile
demos/corba/Makefile
])

