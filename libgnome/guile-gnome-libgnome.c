#include <guile-gnome-libgnome.h>
#include <libgnome/gnome-init.h>

void
init_gnome_libgnome (void)
{
    static gboolean initialized = FALSE;

    if (!initialized) {
	gchar *argv[] = { "guile-gobject", NULL };

	initialized = TRUE;

	gnome_program_init ("guile-gobject", "0.01",
			    &libgnome_module_info, 1,
			    argv, NULL);
    } else
	g_log_set_always_fatal (G_LOG_LEVEL_WARNING | G_LOG_LEVEL_CRITICAL);
}
