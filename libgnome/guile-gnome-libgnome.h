#ifndef __GUILE_GNOME_LIBGNOME_H__
#define __GUILE_GNOME_LIBGNOME_H__

#include <glib/gmacros.h>
#include <libgnome.h>
#include <guile/gh.h>

G_BEGIN_DECLS

void scm_init_gnome_libgnome_module (void);
void init_gnome_libgnome (void);

G_END_DECLS

#endif

