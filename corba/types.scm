(define-module (gnome corba types)
  :use-module (gnome gobject)
  :use-module (oop goops))

(load-extension "libguile-gnome-corba" "init_gnomecorba")

(define (gnome-corba-error format-string . args)
  (save-stack)
  (scm-error 'gnome-corba-error #f format-string args '()))

(%init-gnome-corba-types)

(export gnome-corba-error)

(export %init-gnome-corba-primitives %init-gnome-corba)


