(use-modules (gnome gobject) (gnome gobject primitives) (oop goops))
;;(define <foo> (gtype->class gtype:foo))
;;(define foo (make <foo>))

(debug-enable 'backtrace)
;(display)
;; (define test (gflags-register-static "Test" #((A "a" 1) (B "b" 2) (C "c" 4) (D "d" 8))))
;; (define <test> (gtype->class test))

(define closure (make <gclosure> #:return-type gtype:gulong #:param-types (list gtype:gulong gtype:gchar) #:func (lambda (x y) (* x 5))))
(gclosure-invoke closure 6 #\a)
(define cp (gclosure-primitive-new (lambda (x y) (make <gulong> #:value 8))))
(define args (list (make <gulong> #:value 5) (make <gchar> #:value #\a)))
(define retval (gclosure-primitive-invoke cp gtype:gulong (list->vector args)))

(define gtype:test (gobject-type-register-static gtype:gobject "Test"))

(define-method (gobject:class-init (class <gobject>))
  (display "HELLO WORLD - GOBJECT!") (newline) 
(next-method))

(define <test> (gtype->class gtype:test))

(gobject-class-define-signal <test> 'roswell gtype:void)
(define-method (test:roswell (obj <test>)) (display "ROSWELL") (newline))

(define param (make <gparam-long> #:name 'test))
(gobject-class-install-property <test> param)

(display "PROPERTIES CREATED") (newline)

(define test (make <test>))

(gobject-signal-emit test 'roswell)

;;(define-method (gobject:get-property (obj <test>) (pspec <gtype-instance>))
;;  (display (list "GET-VALUE" obj pspec)) (newline)
;;  (make <glong> #:value 85))


(define-method (gobject:test (obj <test>))
  (display "hello world")(newline))

(gobject-set-property test 'test 2112)
(display "Property test of object test = ")
(display (gobject-get-property test 'test))(newline)

(gobject:test test)

(define bool  (make <gboolean> #:value #f))
 (define uint  (make <guint> #:value 85))
 (define gfloat  (make <gfloat> #:value 3.1415))
 (define chararray  (make <gchararray> #:value "Hello World!"))
(display bool)(newline)
(display chararray)(newline)

(display (gtype-from-name "gint"))(newline)
;(display (gobject-register-type 'GtkObject "GtkObject"))
