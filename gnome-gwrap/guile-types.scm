;;;; 	Copyright (C) 1996,1997,1998 Christopher Lee
;;;; 
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2, or (at your option)
;;;; any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this software; see the file COPYING.  If not, write to
;;;; the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
;;;; 

;; void must be treated specially as a return type, and will not work
;;  as an argument type

(define-module (gnome-gwrap guile-types)
  :use-module (gnome-gwrap g-wrap))

(add-type 'void "void" 
	    ;fn-convert-to-scm 
	    (lambda (x) "SCM_UNSPECIFIED")
	    ;fn-convert-from-scm 
	    (lambda (x) "(void)")
	    ;fn-scm-is-a
	    (lambda (x) 1))

(add-type 'char "char"  
	    ;fn-convert-to-scm 
	    (lambda (x) (list "gh_char2scm(" x ")"))
	    ;fn-convert-from-scm 
	    (lambda (x) (list "gh_scm2char(" x ")"))
	    ;fn-scm-is-a
	    (lambda (x) (list "gh_char_p(" x ")")))

(add-type 'int "int"  
	    ;fn-convert-to-scm 
	    (lambda (x) (list "SCM_MAKINUM(" x ")"))
	    ;fn-convert-from-scm 
	    (lambda (x) (list "SCM_INUM(" x ")"))
	    ;fn-scm-is-a
	    (lambda (x) (list "SCM_INUMP(" x ")")))

;; FIXME: Don't these exact number is-a functions need to check
;; ranges?
(add-type 'unsigned-int "unsigned int"  
          ;; fn-convert-to-scm 
          (lambda (x) (list "gh_ulong2scm(" x ")"))
          ;; fn-convert-from-scm 
          (lambda (x) (list "gh_scm2ulong(" x ")"))
          ;; fn-scm-is-a
          (lambda (x) (list "gh_exact_p(" x ")")))

(add-type 'unsigned-long "unsigned long"  
          ;; fn-convert-to-scm 
          (lambda (x) (list "gh_ulong2scm(" x ")"))
          ;; fn-convert-from-scm 
          (lambda (x) (list "gh_scm2ulong(" x ")"))
          ;; fn-scm-is-a
          (lambda (x) (list "gh_exact_p(" x ")")))

(add-type 'unsigned-long-long "unsigned long long"  
          ;; fn-convert-to-scm 
          (lambda (x) (list "gh_ulonglong2scm(" x ")"))
          ;; fn-convert-from-scm 
          (lambda (x) (list "gh_scm2ulonglong(" x ")"))
          ;; fn-scm-is-a
          (lambda (x) (list "gh_exact_p(" x ")")))

(add-type 'double "double" 
          ;; fn-convert-to-scm 
          (lambda (x) (list "scm_makdbl(" x ",0.0)"))
          ;; fn-convert-from-scm 
          (lambda (x) (list "scm_num2dbl(" x ", \"scm_2_double\")"))
          ;; fn-scm-is-a
          (lambda (x) (list "(SCM_NIMP(" x ") && SCM_REALP(" x "))")))

(add-type 'float "float" 
          ;; fn-convert-to-scm 
          (lambda (x) (list "scm_makdbl((double)" x ",0.0)"))
          ;; fn-convert-from-scm 
          (lambda (x) 
            (list "(float)scm_num2dbl(" x ", \"scm_2_double\")"))
          ;; fn-scm-is-a
          (lambda (x) (list "(SCM_NIMP(" x ") && SCM_REALP(" x "))")))

(add-type 'bool "int" 
          ;; fn-convert-to-scm 
          (lambda (x) (list "((" x ") ? SCM_BOOL_T : SCM_BOOL_F )"))
          ;; fn-convert-from-scm 
          (lambda (x) (list "(SCM_BOOL_F != (" x "))"))
          ;; fn-scm-is-a
          (lambda (x) (list "( SCM_BOOL_T == (" x ") "
                            "|| SCM_BOOL_F == (" x "))")))

;; This type lets you pass symbols to C as strings and get them back
;; as symbols.  Note that the default assumes that C returns freshly
;; malloced strings, so they'll be deleted.  You can override this on
;; a per function basis with the 'no-cleanup option.
(add-new-type
 'string-as-symbol
 (make-complex-c-type
  "char *"
  ;; fn-convert-to-scm 
  (lambda (x) (list "gh_symbol2scm(" x ")"))
  ;; fn-convert-from-scm 
  (lambda (x) (list "gh_symbol2newstr(" x ", NULL)"))
  ;; fn-scm-is-a
  (lambda (x) (list "gh_symbol_p(" x ")"))
  ;; c-cleanup-arg-default?
  #t
  ;; c-cleanup-ret-default?
  #t
  ;; fn-c-cleanup
  (lambda (x) (list "free(" x ")"))))

(add-new-type 
 'const-string
 (make-complex-c-type
  "const char*"
  ;;fn-convert-to-scm 
  (lambda (x) (list "scm_makfrom0str(" x ")"))
  ;;fn-convert-from-scm 
  (lambda (x) (list "g_strdup(SCM_STRING_CHARS(" x "))"))
  ;;fn-scm-is-a
  (lambda (x) (list "(SCM_NIMP(" x ") && SCM_STRINGP(" x "))"))
  ;; c-cleanup-arg-default?
  #f
  ;; c-cleanup-ret-default?
  #f
  ;; fn-c-cleanup
  (lambda (x) (list "free(" x ")"))))

(add-type 'tSCM "SCM" 
	  (lambda (x) x)    ;fn-convert-to-scm 
	  (lambda (x) x)    ;fn-convert-from-scm 
	  (lambda (x) 1))  ;fn-scm-is-a
