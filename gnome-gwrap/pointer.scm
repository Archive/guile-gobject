(define-module (gnome-gwrap pointer)
  :use-module (gnome-gwrap g-wrap))

(new-type
 'POINTER-TOKEN "POINTER_TOKEN" 
 "POINTER_TOKEN_print" "POINTER_TOKEN_free" "POINTER_TOKEN_eq")

(new-type
 'POINTER-ARRAY "POINTER_ARRAY" 
 "POINTER_ARRAY_print" "POINTER_ARRAY_free" "POINTER_ARRAY_eq")

(define-public (make-pointer-token-type name ctype)
  (let ((pt-type (get-type 'POINTER-TOKEN)))
    (add-new-type
     name
     (make-c-type
      ctype
      ;; to-scm-fn
      (lambda (x)
        (list 
         "((" x " == NULL) ? SCM_BOOL_F : "
         (make-conversion-to-scm 
          pt-type
          (list "make_POINTER_TOKEN(\"" name "\"," x "))"))))
      ;; to-foo-fn
      (lambda (x)
	(list 
         "((" x " == SCM_BOOL_F) ? NULL : "
	 "((" ctype ")(" (make-conversion-from-scm pt-type x) ")->pdata))"))
      ;; isa-foo-fn
      (lambda (x) 
	(list
         "((" x " == SCM_BOOL_F) || "
         "((" (make-isa-check pt-type x) ")"
         " && !strcmp(\"" name "\",(" 
         (make-conversion-from-scm pt-type x) ")->typename)))"))))))

(define-public (make-pointer-array-type name ctype item-type)
  (let ((pt-type (get-type 'POINTER-ARRAY)))
    (add-new-type
     name
     (make-c-type
      ctype
      ;; to-scm-fn
      (lambda (x)
        (list
         "((" x " == NULL) ? SCM_BOOL_F : "
         (make-conversion-to-scm 
          pt-type
          (list "make_POINTER_ARRAY(\"" name "\", \""
                item-type "\", (void **) " x "))"))))
      ;; to-foo-fn
      (lambda (x)
	(list 
         "((" x " == SCM_BOOL_F) ? NULL : "
	 "((" ctype ")(" (make-conversion-from-scm pt-type x) ")->pdata))"))
      ;; isa-foo-fn
      (lambda (x) 
	(list
         "((" x " == SCM_BOOL_F) || "
         "((" (make-isa-check pt-type x) ") "
	      " && !strcmp(\"" name "\",(" 
	      (make-conversion-from-scm pt-type x) ")->typename)))"))))))

;;; These end up scoped wrong...

(new-function 
 'pointer-array-ref
 'POINTER-TOKEN "POINTER_ARRAY_ref" '((POINTER-ARRAY a) (int index))
 "Returns the POINTER_TOKEN item at given index in a (zero-indexing).")

(new-function 
 'pointer-array-length
 'unsigned-long "POINTER_ARRAY_length" '((POINTER-ARRAY a))
 "Returns the number of items in a.")
