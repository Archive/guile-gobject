;; $Id$
;;;; 	Copyright (C) 1996,1997,1998 Christopher Lee
;;;; 
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2, or (at your option)
;;;; any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this software; see the file COPYING.  If not, write to
;;;; the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
;;;; 

;; This is a utility for writing a structured text file.
;; 
;; Open a new output file with
;;   (define outfile (make-outfile "file-name" '(list of file sections)))
;; Add text to a section of the file with
;;   (outfile:add-to-section outfile 'section1 "some text here\n")
;; Write the outputfile with
;;   (outfile:close outfile)
;;
;; Function outfile:add-to-section inserts a string or a tree of strings
;;  at the end of a list corresponding to a given section of the file.
;;  When the file is closed with outfile:close, the tree of each section
;;  is traversed (left-to-right), and each string encountered is
;;  display'ed into the file.

(define-module (gnome-gwrap output-file))

(define-public (make-outfile name section-headers)
  (list
   'outfile
   name
   (map (lambda (x) (list x)) section-headers)))

(define (try-resolving-symbols lst sym-assqs)
  (reverse
   (let loop ((l lst)
	      (out '()))
     (cond ((null? l) out)
	   ((symbol? (car l))
	    (let ((a (assq (car l) sym-assqs)))
	      (if a
		  (loop (cdr l) (cons (cadr a) out))
		  (loop (cdr l) (cons (car l) out)))))
	   (else
	    (loop (cdr l) (cons (car l) out)))))))

(define-public (make-outfile-txt sym name text sym-assqs)
  (let* ((tmp-sym (lambda (n)
		    (string->symbol (string-append "$pre"
						   (number->string n)))))
	 (sections
	  (reverse
	   (let loop ((num  0)
		      (secs '())
		      (sec-txt '())
		      (dat 
		       (try-resolving-symbols 
			(grab-str-vars text) sym-assqs)))
	     (cond
	      ((null? dat)
	       (cons (cons (tmp-sym num) (reverse sec-txt)) secs))
	      ((symbol? (car dat))
	       (loop (+ num 1)
		     (append
		      (list (list (car dat) (list))
			    (cons (tmp-sym num) (reverse sec-txt)))
		      secs)
		     '()
		     (cdr dat)))
	      (else
	       (loop num
		     secs
		     (cons (car dat) sec-txt)
		     (cdr dat))))))))
    (list
     'outfile
     name
     (map (lambda (x) (cons (car x) (list->fifo (cdr x))))
	  sections))))
     

(define-public (outfile? x) (eq? (car x) 'outfile))
(define-public (outfile:check x fnname)
  (if (not (outfile? x)) 
      (error (string-append fnname ": item not an outfile"))))
(define-public (outfile:filename-of x)
  (outfile:check x "outfile:filename-of")
  (cadr x))
(define-public (outfile:sections x)
  (outfile:check x "outfile:sections")
  (caddr x))
(define-public (close-outfile x)
  (outfile:check x "close-outfile")
  (set-car! (cdr x) #f))

(define-public (outfile:add-to-section outfile section data)
  (let ((sect-q (assq section (outfile:sections outfile))))
    (if (not sect-q)
	(error "outfile:add-to-section -- section not found " section)
	(set-cdr! sect-q (fifo-add (cdr sect-q) data))))
  #t)

(define-public (outfile:close outfile)
  (if (not (outfile:filename-of outfile))
      (error "outfile already closed"))
  (let ((port (open-output-file (outfile:filename-of outfile))))
    (for-each
     (lambda (section)
       (flatten-display (fifo-get (cdr section)) port))
     (outfile:sections outfile))
    (close-output-port port)
    (close-outfile outfile)))

(define (flatten-display lst port)
  (cond ((null? lst) '())
	((pair? lst) (flatten-display (car lst) port)
		     (flatten-display (cdr lst) port))
	((or (string? lst)
	     (number? lst)
	     (symbol? lst))
	 (display lst port))
	((procedure? lst)
	 (flatten-display ((lst 'output)) port))
	(else
	 (error "flatten-display: bad element found in the tree " lst))))

(define-public (flatten-string lst)
  (cond ((null? lst) "")
	((pair? lst) (string-append
		      (flatten-string (car lst))
		      (flatten-string (cdr lst))))
	((string? lst)
	 lst)
	((number? lst)
	 (number->string lst))
	((symbol? lst)
	 (symbol->string lst))
	((procedure? lst)
	 (flatten-string ((lst 'output))))
	(else
	 (error "flatten-string: bad element found in the tree " lst))))

(define (list->fifo lst)
  (cons lst
	(let loop ((l lst))
	  (cond ((null? l) '())
		((null? (cdr l)) l)
		(loop (cdr l))))))

(define (fifo-add lst x)
  (let ((the-list (list x)))
    (cond ((null? lst) (cons the-list the-list))
	  (else (set-cdr! (cdr lst) the-list)
		(cons (car lst) the-list)))))

(define (fifo-get l)
  (cond ((null? l) ())
	(else (car l))))

(define-public (separate-by lst separator)
  (cond ((null? lst) '())
	((null? (cdr lst)) lst)
	(else
	 (cons (car lst)
	       (cons separator (separate-by (cdr lst) separator))))))

(define (char-idx str char)
  (let ((strlen (string-length str)))
    (let loop ((i 0))
      (cond ((= i strlen) 
	     #f)
	    ((eq? char (string-ref str i))
	     i)
	    (else (loop (+ 1 i)))))))

(define-public (str-translate str charstr transvec)
  (let ((buff-size 10000)
	(str-len (string-length str)))
    (let ((buffer (make-string buff-size))
	  (buff-len 0))
      (let* ((buff-add-char 
	      (lambda (char)
		(cond ((= buff-size buff-len)
		       (set! buffer (string-append buffer 
						   (make-string buff-size)))
		       (set! buff-size (* 2 buff-size))))
		(string-set! buffer buff-len char)
		(set! buff-len (+ 1 buff-len))))
	     (buff-add-string
	      (lambda (string)
		(let ((stringlen (string-length string)))
		  (do ((i 0 (+ i 1))) ((= i stringlen))
		    (buff-add-char (string-ref string i)))))))
	(let loop ((i 0))
	  (cond ((= i str-len)
		 (substring buffer 0 buff-len))
		((char-idx charstr (string-ref str i))
		 => (lambda (index)
		      (buff-add-string (vector-ref transvec index))
		      (loop (+ 1 i))))
		(else
		 (buff-add-char (string-ref str i))
		 (loop (+ 1 i)))))))))
	       



;; (grab-str-vars "void (%this%*)(%<SCM>%,%<generic-data-pointer>%);")
;;   --> ("void (" this "*)(" <SCM> "," <generic-data-pointer> ");")
;; (grab-str-vars "%this% is a %variable% and %%this%% is not!")
;;   --> ("" this " is a " variable " and %" "this%" " is not!")
(define-public (grab-str-vars str)
  (let ((len (string-length str)))
    (reverse
     (let out-loop ((result '()) ;; outer loop looks for %variables%
		    (start 0)
		    (end 0))
       (cond ((= end len)        ;; end of string
	      (cons (substring str start end) result))
	     ((eq? #\% (string-ref str end)) ;; first % -- start of var?
	      (cond ((= (+ 1 end) len)                    ;; % is last char
		     (out-loop result start (+ 1 end)))
		    ((eq? #\% (string-ref str (+ 1 end))) ;; allow %% -> %
		     (out-loop (cons (substring str start (+ 1 end)) result)
			       (+ 2 end) (+ 2 end)))
		    (else                    ;; % marks start of var
		     (let inn-loop ((var-start (+ end 1)) ;; inner loop finds
				    (var-end (+ end 1)))  ;;   end of %vars%
		       (cond ((= var-end len) ;; string ends before 2nd %
			      (cons (substring str start var-end) result))
			     ((eq? #\% (string-ref str var-end))
			      (out-loop       ;; end of %var%
			       (cons (string->symbol 
				      (substring str var-start var-end))
				     (cons (substring str start end)
					   result))
			       (+ var-end 1) (+ var-end 1)))
			     (else
			      (inn-loop var-start (+ var-end 1))))))))
	     (else
	      (out-loop result start (+ end 1))))))))

(define-public (decode-declaration str symbol-trans-proc)
  (apply string-append
	 (map 
	  (lambda (x)
	    (cond ((symbol? x) (symbol-trans-proc x))
		  ((string? x) x)
		  (else "?????")))
	  (grab-str-vars str))))


(define-public (translate-vars lst xlate-assq)
  (cond
   ((null? lst) 
    '())
   ((pair? lst) 
    (cons (translate-vars (car lst) xlate-assq)
	  (translate-vars (cdr lst) xlate-assq)))
   ((symbol? lst)
    (let ((asq-pair (assq lst xlate-assq)))
      (cond
       ((not asq-pair)
	(error "translate-vars: no translation for" lst))
       (else
	(cadr asq-pair)))))
   (else
    lst)))

(define-public (translate-str str xlate-assq)
  (translate-vars (grab-str-vars str) xlate-assq))
