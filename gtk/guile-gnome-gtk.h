#ifndef __GUILE_GNOME_GTK_H__
#define __GUILE_GNOME_GTK_H__

#include <libguile.h>
#include <gobject/guile-gnome-gobject.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

void scm_init_gnome_gtk_module (void);
void scm_init_gnome_gtk (void);

SCM scm_my_gtk_test (void);

G_END_DECLS

#endif
