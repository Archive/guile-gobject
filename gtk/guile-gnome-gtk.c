#include <gtk/guile-gnome-gtk.h>
#include <gobject/guile-gnome-gobject.h>
#include <gtk/guile-gnome-gtk-aux.h>
#include <gtk/gtkwindow.h>
#include <gtk/gtklabel.h>

SCM
scm_my_gtk_test (void)
{
    g_message (G_STRLOC);

    return SCM_UNSPECIFIED;
}

void
scm_init_gnome_gtk (void)
{
    gchar **argv;
    gint argc = 1;

    argv = g_new0 (gchar *, 2);
    argv[0] = g_strdup ("(guile-gnome-gtk)");


    gtk_init (&argc, &argv);

    scm_init_gnome_gtk_aux ();

/*
    g_type_class_ref (GTK_TYPE_WIDGET);
    g_type_class_ref (GTK_TYPE_WINDOW);
    g_type_class_ref (GTK_TYPE_LABEL);
    g_type_class_ref (GTK_TYPE_MISC);
    g_type_class_ref (GTK_TYPE_BUTTON);
    g_type_class_ref (GTK_TYPE_BOX);
    g_type_class_ref (GTK_TYPE_HBOX);
    g_type_class_ref (GTK_TYPE_VBOX);
    
    gtk_widget_get_type ();
    gtk_window_get_type ();
    gtk_label_get_type ();
    gtk_button_get_type ();
    gtk_box_get_type ();
    gtk_hbox_get_type ();
    gtk_vbox_get_type ();
*/

}

