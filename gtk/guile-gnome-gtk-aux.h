#ifndef __GUILE_GNOME_GTK_AUX_H__
#define __GUILE_GNOME_GTK_AUX_H__

#include <libguile.h>
#include <gobject/guile-gnome-gobject.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

void scm_init_gnome_gtk_aux (void);


G_END_DECLS

#endif
