#!/usr/bin/perl

open (FILE, $ARGV[0]) or die;

read (FILE, $str, 10000000);

close FILE;

$_ = $str;

die unless open (FUNC,">>gnome-gtk.gwp");

#print something as header

print FUNC ';;; -*-scheme-*-
                                                                                                                                        
(use-modules (gnome-gwrap g-wrap)
             (gnome-gwrap guile-types))
                                                                                                                                        
(gwrap-open-module "gnomegtk"
                   \'(guile-module (gnome gtk))
                   \'(call-on-init scm_init_gnome_gtk_module))
                  
(gwrap-include-global-header "guile-gnome-gtk.h")
                                                                                     

(define-macro (add-gobject-type name type)
  `(add-type ,name (string-append (symbol->string ,name) " *")
	     (lambda (x) (list "scm_c_gtype_instance_to_scm ((GtypeInstance *) " x ")"))
	     (lambda (x) (list "(" (symbol->string ,name) " *) scm_c_scm_to_gtype_instance (" x ", " ,type ")"))
	     (lambda (x) (list "scm_c_gtype_instance_is_a_p (" x ", " ,type ")"))))

;;Adding new types


';


while(/\(define-object\s+\w+\s+\(in-module\s+\"\w+"\)\s+\(parent\s+\"\w+"\)\s+\(c-name\s+\"(\w+)"\)\s+\(gtype-id\s+\"(\w+)"\)/goi){ 

    print FUNC "(add-gobject-type '$1 '$2)\n";

}

print FUNC "\n(gwrap-close-module)\n";

close FUNC;
exit;
#stuff metodos

open (FILE, $ARGV[1]) or die;

read (FILE, $str, 10000000);

close FILE;

$_ = $str;

#if (/\(define-method\s+\w+\s+\([-\w]+\s+\"\w+"\)\s+/goi){
#if (/\(define-method\s+\w+\s+(\([-\w]+\s+\"\w+"\)\s+|(c-name\s+\"(\w+)"\)\s+)+/goi){
#if (/\(define-method\s+\w+\s+[-\(\)\w\s"]+\n\)/goi){


sub isMethod {
    my($str) = $_[0];
    local($_) = $str;

    if (/method/i){
	return 1;
    }

    return 0;
}

sub getCName {
    my($str) = $_[0];
    local($_) = $str;
    if (/\(c-name\s+\"(\w+)"\)\s+/goi){
      if (substr($1,0,0) eq "_"){
          return substr ($1, 1);
         }
      else { return $1 }
}
      return 0;
}

sub getSchemeName {
    my($str) = $_[0];
    local($_) = $str;
    my($a) = "_";
    my ($b) = "-";
    s/$a/$b/goie;
    $_;
}
sub getParameters {
    my($str) = $_[0];

    local($_) = $str;

    @gtype = ();
    @name = ();

    while (/\'\("([\w\*\-]+)"\s+"(\w+)"/goi){
	   $gtype[$#gtype+1] = $1;
	   $name[$#name+1] = $2;
	   print FUNC " ($1 $2) \n";
       }
    

}

sub getReturnType {
    my($str) = $_[0];
    local ($_) = $str;
    if (/\(return-type\s+\"([\w\*]+)"\)\s+/goi){
      if ($1 eq 'none'){
          return 'void';
     }
     return $1;
    }
      return 0;
}

    @array = split (/\n\(define-/, $str);

foreach $elem (@array){
  if (/\(define-method/goi){
     if ($cname = &getCName ($elem)){
    print $cname . "\n";
     print FUNC "\n(new-function \"" . &getSchemeName ($cname) . "\"\n" . 
                "'" . &getReturnType ($elem) . "\n" .
                '"' .  $cname . "\"\n" .
                "'("  ; 
      &getParameters ($elem) ;
      print FUNC ")\n" . "nil)\n";
      }
    }
}

print FUNC "\n(gwrap-close-module)\n";

close FUNC;

