#!/usr/bin/perl

open (FILE, $ARGV[0]) or die;

read (FILE, $str, 10000000);

close FILE;

$_ = $str;

@gtktype = ();
@gtkclass = ();

die unless open (DEFS,">>gtk.scm");
die unless open (CAUX,">>guile-gnome-gtk-aux.c");

print DEFS '(define-module (gnome gtk)
  :use-module (gnome gobject)
  :use-module (gnome gobject primitives)
  :use-module (oop goops))
                                                                                                                                        
(load-extension "libguile-gnome-gtk" "init_gnomegtk")
                                                                                                                                        
(%init-gnome-gtk)

';

print CAUX "
#include <gtk/guile-gnome-gtk.h>
#include <gobject/guile-gnome-gobject.h>
#include <gtk/guile-gnome-gtk-aux.h>

void 
scm_init_gnome_gtk_aux (){

";


while(/\(define-object\s+\w+\s+\(in-module\s+\"\w+"\)\s+\(parent\s+\"\w+"\)\s+\(c-name\s+\"(\w+)"\)\s+\(gtype-id\s+\"(\w+)"\)/goi){ 
 my($lower) = lc ($1);
 my($type) = "gtktype:$lower";
 my($class) = uc ($1);
 my ($underscore) = $2;
 my ($func) = substr (lc ($underscore), 9, length(lc($underscore)));

 print DEFS "\n(define $type (gtype-from-name \"$1\"))\n";
 print DEFS "\n(define <$class> (gtype->class gtktype:$lower))\n";
 $gtktype[$#gtktype+1] = $type;
 $gtkclass[$#gtkclass+1] = $class;

   print CAUX "\t\tg_type_class_ref ($underscore);\n";
   print CAUX "\t\tgtk_" . $func . "_get_type ();\n";


}

print CAUX "}\n";

print DEFS "\n(export "; 

foreach $mod (@gtktype){
 print DEFS "\t\t$mod\n";
}

foreach $mod (@gtkclass){
  print DEFS "\t\t<$mod>\n";
}

print DEFS ')';

close DEFS;
close CAUX;
