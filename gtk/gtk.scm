(define-module (gnome gtk)
  :use-module (gnome gobject)
  :use-module (gnome gobject primitives)
  :use-module (oop goops))
                                                                                                                                        
(load-extension "libguile-gnome-gtk" "init_gnomegtk")
                                                                                                                                        
(%init-gnome-gtk)


(define gtktype:gtkaccelgroup (gtype-from-name "GtkAccelGroup"))

(define <GTKACCELGROUP> (gtype->class gtktype:gtkaccelgroup))

(define gtktype:gtkaccessible (gtype-from-name "GtkAccessible"))

(define <GTKACCESSIBLE> (gtype->class gtktype:gtkaccessible))

(define gtktype:gtkiconfactory (gtype-from-name "GtkIconFactory"))

(define <GTKICONFACTORY> (gtype->class gtktype:gtkiconfactory))

(define gtktype:gtkobject (gtype-from-name "GtkObject"))

(define <GTKOBJECT> (gtype->class gtktype:gtkobject))

(define gtktype:gtkitemfactory (gtype-from-name "GtkItemFactory"))

(define <GTKITEMFACTORY> (gtype->class gtktype:gtkitemfactory))

(define gtktype:gtkimcontext (gtype-from-name "GtkIMContext"))

(define <GTKIMCONTEXT> (gtype->class gtktype:gtkimcontext))

(define gtktype:gtkimcontextsimple (gtype-from-name "GtkIMContextSimple"))

(define <GTKIMCONTEXTSIMPLE> (gtype->class gtktype:gtkimcontextsimple))

(define gtktype:gtkimmulticontext (gtype-from-name "GtkIMMulticontext"))

(define <GTKIMMULTICONTEXT> (gtype->class gtktype:gtkimmulticontext))

(define gtktype:gtkcellrenderer (gtype-from-name "GtkCellRenderer"))

(define <GTKCELLRENDERER> (gtype->class gtktype:gtkcellrenderer))

(define gtktype:gtkcellrenderertoggle (gtype-from-name "GtkCellRendererToggle"))

(define <GTKCELLRENDERERTOGGLE> (gtype->class gtktype:gtkcellrenderertoggle))

(define gtktype:gtkcellrenderertext (gtype-from-name "GtkCellRendererText"))

(define <GTKCELLRENDERERTEXT> (gtype->class gtktype:gtkcellrenderertext))

(define gtktype:gtkcellrendererpixbuf (gtype-from-name "GtkCellRendererPixbuf"))

(define <GTKCELLRENDERERPIXBUF> (gtype->class gtktype:gtkcellrendererpixbuf))

(define gtktype:gtkadjustment (gtype-from-name "GtkAdjustment"))

(define <GTKADJUSTMENT> (gtype->class gtktype:gtkadjustment))

(define gtktype:gtkrcstyle (gtype-from-name "GtkRcStyle"))

(define <GTKRCSTYLE> (gtype->class gtktype:gtkrcstyle))

(define gtktype:gtksettings (gtype-from-name "GtkSettings"))

(define <GTKSETTINGS> (gtype->class gtktype:gtksettings))

(define gtktype:gtksizegroup (gtype-from-name "GtkSizeGroup"))

(define <GTKSIZEGROUP> (gtype->class gtktype:gtksizegroup))

(define gtktype:gtktextbuffer (gtype-from-name "GtkTextBuffer"))

(define <GTKTEXTBUFFER> (gtype->class gtktype:gtktextbuffer))

(define gtktype:gtktextchildanchor (gtype-from-name "GtkTextChildAnchor"))

(define <GTKTEXTCHILDANCHOR> (gtype->class gtktype:gtktextchildanchor))

(define gtktype:gtktextmark (gtype-from-name "GtkTextMark"))

(define <GTKTEXTMARK> (gtype->class gtktype:gtktextmark))

(define gtktype:gtktexttag (gtype-from-name "GtkTextTag"))

(define <GTKTEXTTAG> (gtype->class gtktype:gtktexttag))

(define gtktype:gtktexttagtable (gtype-from-name "GtkTextTagTable"))

(define <GTKTEXTTAGTABLE> (gtype->class gtktype:gtktexttagtable))

(define gtktype:gtktooltips (gtype-from-name "GtkTooltips"))

(define <GTKTOOLTIPS> (gtype->class gtktype:gtktooltips))

(define gtktype:gtkliststore (gtype-from-name "GtkListStore"))

(define <GTKLISTSTORE> (gtype->class gtktype:gtkliststore))

(define gtktype:gtktreemodelsort (gtype-from-name "GtkTreeModelSort"))

(define <GTKTREEMODELSORT> (gtype->class gtktype:gtktreemodelsort))

(define gtktype:gtktreeselection (gtype-from-name "GtkTreeSelection"))

(define <GTKTREESELECTION> (gtype->class gtktype:gtktreeselection))

(define gtktype:gtktreestore (gtype-from-name "GtkTreeStore"))

(define <GTKTREESTORE> (gtype->class gtktype:gtktreestore))

(define gtktype:gtktreeviewcolumn (gtype-from-name "GtkTreeViewColumn"))

(define <GTKTREEVIEWCOLUMN> (gtype->class gtktype:gtktreeviewcolumn))

(define gtktype:gtkwidget (gtype-from-name "GtkWidget"))

(define <GTKWIDGET> (gtype->class gtktype:gtkwidget))

(define gtktype:gtkseparator (gtype-from-name "GtkSeparator"))

(define <GTKSEPARATOR> (gtype->class gtktype:gtkseparator))

(define gtktype:gtkvseparator (gtype-from-name "GtkVSeparator"))

(define <GTKVSEPARATOR> (gtype->class gtktype:gtkvseparator))

(define gtktype:gtkhseparator (gtype-from-name "GtkHSeparator"))

(define <GTKHSEPARATOR> (gtype->class gtktype:gtkhseparator))

(define gtktype:gtkruler (gtype-from-name "GtkRuler"))

(define <GTKRULER> (gtype->class gtktype:gtkruler))

(define gtktype:gtkvruler (gtype-from-name "GtkVRuler"))

(define <GTKVRULER> (gtype->class gtktype:gtkvruler))

(define gtktype:gtkhruler (gtype-from-name "GtkHRuler"))

(define <GTKHRULER> (gtype->class gtktype:gtkhruler))

(define gtktype:gtkrange (gtype-from-name "GtkRange"))

(define <GTKRANGE> (gtype->class gtktype:gtkrange))

(define gtktype:gtkscrollbar (gtype-from-name "GtkScrollbar"))

(define <GTKSCROLLBAR> (gtype->class gtktype:gtkscrollbar))

(define gtktype:gtkvscrollbar (gtype-from-name "GtkVScrollbar"))

(define <GTKVSCROLLBAR> (gtype->class gtktype:gtkvscrollbar))

(define gtktype:gtkhscrollbar (gtype-from-name "GtkHScrollbar"))

(define <GTKHSCROLLBAR> (gtype->class gtktype:gtkhscrollbar))

(define gtktype:gtkscale (gtype-from-name "GtkScale"))

(define <GTKSCALE> (gtype->class gtktype:gtkscale))

(define gtktype:gtkvscale (gtype-from-name "GtkVScale"))

(define <GTKVSCALE> (gtype->class gtktype:gtkvscale))

(define gtktype:gtkhscale (gtype-from-name "GtkHScale"))

(define <GTKHSCALE> (gtype->class gtktype:gtkhscale))

(define gtktype:gtkprogress (gtype-from-name "GtkProgress"))

(define <GTKPROGRESS> (gtype->class gtktype:gtkprogress))

(define gtktype:gtkprogressbar (gtype-from-name "GtkProgressBar"))

(define <GTKPROGRESSBAR> (gtype->class gtktype:gtkprogressbar))

(define gtktype:gtkpreview (gtype-from-name "GtkPreview"))

(define <GTKPREVIEW> (gtype->class gtktype:gtkpreview))

(define gtktype:gtkoldeditable (gtype-from-name "GtkOldEditable"))

(define <GTKOLDEDITABLE> (gtype->class gtktype:gtkoldeditable))

(define gtktype:gtkmisc (gtype-from-name "GtkMisc"))

(define <GTKMISC> (gtype->class gtktype:gtkmisc))

(define gtktype:gtkpixmap (gtype-from-name "GtkPixmap"))

(define <GTKPIXMAP> (gtype->class gtktype:gtkpixmap))

(define gtktype:gtkarrow (gtype-from-name "GtkArrow"))

(define <GTKARROW> (gtype->class gtktype:gtkarrow))

(define gtktype:gtkimage (gtype-from-name "GtkImage"))

(define <GTKIMAGE> (gtype->class gtktype:gtkimage))

(define gtktype:gtklabel (gtype-from-name "GtkLabel"))

(define <GTKLABEL> (gtype->class gtktype:gtklabel))

(define gtktype:gtkaccellabel (gtype-from-name "GtkAccelLabel"))

(define <GTKACCELLABEL> (gtype->class gtktype:gtkaccellabel))

(define gtktype:gtkinvisible (gtype-from-name "GtkInvisible"))

(define <GTKINVISIBLE> (gtype->class gtktype:gtkinvisible))

;;(define gtktype:gtkentry (gtype-from-name "GtkEntry"))

;;(define <GTKENTRY> (gtype->class gtktype:gtkentry))

;;(define gtktype:gtkspinbutton (gtype-from-name "GtkSpinButton"))

;;(define <GTKSPINBUTTON> (gtype->class gtktype:gtkspinbutton))

(define gtktype:gtkdrawingarea (gtype-from-name "GtkDrawingArea"))

(define <GTKDRAWINGAREA> (gtype->class gtktype:gtkdrawingarea))

(define gtktype:gtkcurve (gtype-from-name "GtkCurve"))

(define <GTKCURVE> (gtype->class gtktype:gtkcurve))

(define gtktype:gtkcontainer (gtype-from-name "GtkContainer"))

(define <GTKCONTAINER> (gtype->class gtktype:gtkcontainer))

(define gtktype:gtktreeview (gtype-from-name "GtkTreeView"))

(define <GTKTREEVIEW> (gtype->class gtktype:gtktreeview))

(define gtktype:gtktoolbar (gtype-from-name "GtkToolbar"))

(define <GTKTOOLBAR> (gtype->class gtktype:gtktoolbar))

(define gtktype:gtktextview (gtype-from-name "GtkTextView"))

(define <GTKTEXTVIEW> (gtype->class gtktype:gtktextview))

(define gtktype:gtktable (gtype-from-name "GtkTable"))

(define <GTKTABLE> (gtype->class gtktype:gtktable))

(define gtktype:gtksocket (gtype-from-name "GtkSocket"))

(define <GTKSOCKET> (gtype->class gtktype:gtksocket))

(define gtktype:gtkpaned (gtype-from-name "GtkPaned"))

(define <GTKPANED> (gtype->class gtktype:gtkpaned))

(define gtktype:gtkvpaned (gtype-from-name "GtkVPaned"))

(define <GTKVPANED> (gtype->class gtktype:gtkvpaned))

(define gtktype:gtkhpaned (gtype-from-name "GtkHPaned"))

(define <GTKHPANED> (gtype->class gtktype:gtkhpaned))

(define gtktype:gtknotebook (gtype-from-name "GtkNotebook"))

(define <GTKNOTEBOOK> (gtype->class gtktype:gtknotebook))

(define gtktype:gtkmenushell (gtype-from-name "GtkMenuShell"))

(define <GTKMENUSHELL> (gtype->class gtktype:gtkmenushell))

(define gtktype:gtkmenu (gtype-from-name "GtkMenu"))

(define <GTKMENU> (gtype->class gtktype:gtkmenu))

(define gtktype:gtkmenubar (gtype-from-name "GtkMenuBar"))

(define <GTKMENUBAR> (gtype->class gtktype:gtkmenubar))

(define gtktype:gtklist (gtype-from-name "GtkList"))

(define <GTKLIST> (gtype->class gtktype:gtklist))

(define gtktype:gtklayout (gtype-from-name "GtkLayout"))

(define <GTKLAYOUT> (gtype->class gtktype:gtklayout))

(define gtktype:gtkfixed (gtype-from-name "GtkFixed"))

(define <GTKFIXED> (gtype->class gtktype:gtkfixed))

(define gtktype:gtkbin (gtype-from-name "GtkBin"))

(define <GTKBIN> (gtype->class gtktype:gtkbin))

(define gtktype:gtkviewport (gtype-from-name "GtkViewport"))

(define <GTKVIEWPORT> (gtype->class gtktype:gtkviewport))

(define gtktype:gtkscrolledwindow (gtype-from-name "GtkScrolledWindow"))

(define <GTKSCROLLEDWINDOW> (gtype->class gtktype:gtkscrolledwindow))

(define gtktype:gtkitem (gtype-from-name "GtkItem"))

(define <GTKITEM> (gtype->class gtktype:gtkitem))

(define gtktype:gtkmenuitem (gtype-from-name "GtkMenuItem"))

(define <GTKMENUITEM> (gtype->class gtktype:gtkmenuitem))

(define gtktype:gtktearoffmenuitem (gtype-from-name "GtkTearoffMenuItem"))

(define <GTKTEAROFFMENUITEM> (gtype->class gtktype:gtktearoffmenuitem))

(define gtktype:gtkseparatormenuitem (gtype-from-name "GtkSeparatorMenuItem"))

(define <GTKSEPARATORMENUITEM> (gtype->class gtktype:gtkseparatormenuitem))

(define gtktype:gtkcheckmenuitem (gtype-from-name "GtkCheckMenuItem"))

(define <GTKCHECKMENUITEM> (gtype->class gtktype:gtkcheckmenuitem))

(define gtktype:gtkradiomenuitem (gtype-from-name "GtkRadioMenuItem"))

(define <GTKRADIOMENUITEM> (gtype->class gtktype:gtkradiomenuitem))

(define gtktype:gtkimagemenuitem (gtype-from-name "GtkImageMenuItem"))

(define <GTKIMAGEMENUITEM> (gtype->class gtktype:gtkimagemenuitem))

(define gtktype:gtklistitem (gtype-from-name "GtkListItem"))

(define <GTKLISTITEM> (gtype->class gtktype:gtklistitem))

(define gtktype:gtkhandlebox (gtype-from-name "GtkHandleBox"))

(define <GTKHANDLEBOX> (gtype->class gtktype:gtkhandlebox))

(define gtktype:gtkframe (gtype-from-name "GtkFrame"))

(define <GTKFRAME> (gtype->class gtktype:gtkframe))

(define gtktype:gtkaspectframe (gtype-from-name "GtkAspectFrame"))

(define <GTKASPECTFRAME> (gtype->class gtktype:gtkaspectframe))

(define gtktype:gtkeventbox (gtype-from-name "GtkEventBox"))

(define <GTKEVENTBOX> (gtype->class gtktype:gtkeventbox))

(define gtktype:gtkalignment (gtype-from-name "GtkAlignment"))

(define <GTKALIGNMENT> (gtype->class gtktype:gtkalignment))

(define gtktype:gtkbutton (gtype-from-name "GtkButton"))

(define <GTKBUTTON> (gtype->class gtktype:gtkbutton))

(define gtktype:gtktogglebutton (gtype-from-name "GtkToggleButton"))

(define <GTKTOGGLEBUTTON> (gtype->class gtktype:gtktogglebutton))

(define gtktype:gtkcheckbutton (gtype-from-name "GtkCheckButton"))

(define <GTKCHECKBUTTON> (gtype->class gtktype:gtkcheckbutton))

(define gtktype:gtkradiobutton (gtype-from-name "GtkRadioButton"))

(define <GTKRADIOBUTTON> (gtype->class gtktype:gtkradiobutton))

(define gtktype:gtkoptionmenu (gtype-from-name "GtkOptionMenu"))

(define <GTKOPTIONMENU> (gtype->class gtktype:gtkoptionmenu))

(define gtktype:gtkbox (gtype-from-name "GtkBox"))

(define <GTKBOX> (gtype->class gtktype:gtkbox))

(define gtktype:gtkvbox (gtype-from-name "GtkVBox"))

(define <GTKVBOX> (gtype->class gtktype:gtkvbox))

(define gtktype:gtkcolorselection (gtype-from-name "GtkColorSelection"))

(define <GTKCOLORSELECTION> (gtype->class gtktype:gtkcolorselection))

(define gtktype:gtkfontselection (gtype-from-name "GtkFontSelection"))

(define <GTKFONTSELECTION> (gtype->class gtktype:gtkfontselection))

(define gtktype:gtkgammacurve (gtype-from-name "GtkGammaCurve"))

(define <GTKGAMMACURVE> (gtype->class gtktype:gtkgammacurve))

(define gtktype:gtkhbox (gtype-from-name "GtkHBox"))

(define <GTKHBOX> (gtype->class gtktype:gtkhbox))

(define gtktype:gtkstatusbar (gtype-from-name "GtkStatusbar"))

(define <GTKSTATUSBAR> (gtype->class gtktype:gtkstatusbar))

(define gtktype:gtkcombo (gtype-from-name "GtkCombo"))

(define <GTKCOMBO> (gtype->class gtktype:gtkcombo))

(define gtktype:gtkbuttonbox (gtype-from-name "GtkButtonBox"))

(define <GTKBUTTONBOX> (gtype->class gtktype:gtkbuttonbox))

(define gtktype:gtkvbuttonbox (gtype-from-name "GtkVButtonBox"))

(define <GTKVBUTTONBOX> (gtype->class gtktype:gtkvbuttonbox))

(define gtktype:gtkhbuttonbox (gtype-from-name "GtkHButtonBox"))

(define <GTKHBUTTONBOX> (gtype->class gtktype:gtkhbuttonbox))

(define gtktype:gtkclist (gtype-from-name "GtkCList"))

(define <GTKCLIST> (gtype->class gtktype:gtkclist))

(define gtktype:gtkctree (gtype-from-name "GtkCTree"))

(define <GTKCTREE> (gtype->class gtktype:gtkctree))

(define gtktype:gtkcalendar (gtype-from-name "GtkCalendar"))

(define <GTKCALENDAR> (gtype->class gtktype:gtkcalendar))

(define gtktype:gtkwindow (gtype-from-name "GtkWindow"))

(define <GTKWINDOW> (gtype->class gtktype:gtkwindow))

(define gtktype:gtkplug (gtype-from-name "GtkPlug"))

(define <GTKPLUG> (gtype->class gtktype:gtkplug))

(define gtktype:gtkdialog (gtype-from-name "GtkDialog"))

(define <GTKDIALOG> (gtype->class gtktype:gtkdialog))

(define gtktype:gtkmessagedialog (gtype-from-name "GtkMessageDialog"))

(define <GTKMESSAGEDIALOG> (gtype->class gtktype:gtkmessagedialog))

(define gtktype:gtkinputdialog (gtype-from-name "GtkInputDialog"))

(define <GTKINPUTDIALOG> (gtype->class gtktype:gtkinputdialog))

(define gtktype:gtkfontselectiondialog (gtype-from-name "GtkFontSelectionDialog"))

(define <GTKFONTSELECTIONDIALOG> (gtype->class gtktype:gtkfontselectiondialog))

(define gtktype:gtkfileselection (gtype-from-name "GtkFileSelection"))

(define <GTKFILESELECTION> (gtype->class gtktype:gtkfileselection))

(define gtktype:gtkcolorselectiondialog (gtype-from-name "GtkColorSelectionDialog"))

(define <GTKCOLORSELECTIONDIALOG> (gtype->class gtktype:gtkcolorselectiondialog))

(define gtktype:gtkwindowgroup (gtype-from-name "GtkWindowGroup"))

(define <GTKWINDOWGROUP> (gtype->class gtktype:gtkwindowgroup))

(export 		gtktype:gtkaccelgroup
		gtktype:gtkaccessible
		gtktype:gtkiconfactory
		gtktype:gtkobject
		gtktype:gtkitemfactory
		gtktype:gtkimcontext
		gtktype:gtkimcontextsimple
		gtktype:gtkimmulticontext
		gtktype:gtkcellrenderer
		gtktype:gtkcellrenderertoggle
		gtktype:gtkcellrenderertext
		gtktype:gtkcellrendererpixbuf
		gtktype:gtkadjustment
		gtktype:gtkrcstyle
		gtktype:gtksettings
		gtktype:gtksizegroup
		gtktype:gtktextbuffer
		gtktype:gtktextchildanchor
		gtktype:gtktextmark
		gtktype:gtktexttag
		gtktype:gtktexttagtable
		gtktype:gtktooltips
		gtktype:gtkliststore
		gtktype:gtktreemodelsort
		gtktype:gtktreeselection
		gtktype:gtktreestore
		gtktype:gtktreeviewcolumn
		gtktype:gtkwidget
		gtktype:gtkseparator
		gtktype:gtkvseparator
		gtktype:gtkhseparator
		gtktype:gtkruler
		gtktype:gtkvruler
		gtktype:gtkhruler
		gtktype:gtkrange
		gtktype:gtkscrollbar
		gtktype:gtkvscrollbar
		gtktype:gtkhscrollbar
		gtktype:gtkscale
		gtktype:gtkvscale
		gtktype:gtkhscale
		gtktype:gtkprogress
		gtktype:gtkprogressbar
		gtktype:gtkpreview
		gtktype:gtkoldeditable
		gtktype:gtkmisc
		gtktype:gtkpixmap
		gtktype:gtkarrow
		gtktype:gtkimage
		gtktype:gtklabel
		gtktype:gtkaccellabel
		gtktype:gtkinvisible
		;;gtktype:gtkentry
		;;gtktype:gtkspinbutton
		gtktype:gtkdrawingarea
		gtktype:gtkcurve
		gtktype:gtkcontainer
		gtktype:gtktreeview
		gtktype:gtktoolbar
		gtktype:gtktextview
		gtktype:gtktable
		gtktype:gtksocket
		gtktype:gtkpaned
		gtktype:gtkvpaned
		gtktype:gtkhpaned
		gtktype:gtknotebook
		gtktype:gtkmenushell
		gtktype:gtkmenu
		gtktype:gtkmenubar
		gtktype:gtklist
		gtktype:gtklayout
		gtktype:gtkfixed
		gtktype:gtkbin
		gtktype:gtkviewport
		gtktype:gtkscrolledwindow
		gtktype:gtkitem
		gtktype:gtkmenuitem
		gtktype:gtktearoffmenuitem
		gtktype:gtkseparatormenuitem
		gtktype:gtkcheckmenuitem
		gtktype:gtkradiomenuitem
		gtktype:gtkimagemenuitem
		gtktype:gtklistitem
		gtktype:gtkhandlebox
		gtktype:gtkframe
		gtktype:gtkaspectframe
		gtktype:gtkeventbox
		gtktype:gtkalignment
		gtktype:gtkbutton
		gtktype:gtktogglebutton
		gtktype:gtkcheckbutton
		gtktype:gtkradiobutton
		gtktype:gtkoptionmenu
		gtktype:gtkbox
		gtktype:gtkvbox
		gtktype:gtkcolorselection
		gtktype:gtkfontselection
		gtktype:gtkgammacurve
		gtktype:gtkhbox
		gtktype:gtkstatusbar
		gtktype:gtkcombo
		gtktype:gtkbuttonbox
		gtktype:gtkvbuttonbox
		gtktype:gtkhbuttonbox
		gtktype:gtkclist
		gtktype:gtkctree
		gtktype:gtkcalendar
		gtktype:gtkwindow
		gtktype:gtkplug
		gtktype:gtkdialog
		gtktype:gtkmessagedialog
		gtktype:gtkinputdialog
		gtktype:gtkfontselectiondialog
		gtktype:gtkfileselection
		gtktype:gtkcolorselectiondialog
		gtktype:gtkwindowgroup
		<GTKACCELGROUP>
		<GTKACCESSIBLE>
		<GTKICONFACTORY>
		<GTKOBJECT>
		<GTKITEMFACTORY>
		<GTKIMCONTEXT>
		<GTKIMCONTEXTSIMPLE>
		<GTKIMMULTICONTEXT>
		<GTKCELLRENDERER>
		<GTKCELLRENDERERTOGGLE>
		<GTKCELLRENDERERTEXT>
		<GTKCELLRENDERERPIXBUF>
		<GTKADJUSTMENT>
		<GTKRCSTYLE>
		<GTKSETTINGS>
		<GTKSIZEGROUP>
		<GTKTEXTBUFFER>
		<GTKTEXTCHILDANCHOR>
		<GTKTEXTMARK>
		<GTKTEXTTAG>
		<GTKTEXTTAGTABLE>
		<GTKTOOLTIPS>
		<GTKLISTSTORE>
		<GTKTREEMODELSORT>
		<GTKTREESELECTION>
		<GTKTREESTORE>
		<GTKTREEVIEWCOLUMN>
		<GTKWIDGET>
		<GTKSEPARATOR>
		<GTKVSEPARATOR>
		<GTKHSEPARATOR>
		<GTKRULER>
		<GTKVRULER>
		<GTKHRULER>
		<GTKRANGE>
		<GTKSCROLLBAR>
		<GTKVSCROLLBAR>
		<GTKHSCROLLBAR>
		<GTKSCALE>
		<GTKVSCALE>
		<GTKHSCALE>
		<GTKPROGRESS>
		<GTKPROGRESSBAR>
		<GTKPREVIEW>
		<GTKOLDEDITABLE>
		<GTKMISC>
		<GTKPIXMAP>
		<GTKARROW>
		<GTKIMAGE>
		<GTKLABEL>
		<GTKACCELLABEL>
		<GTKINVISIBLE>
		;;<GTKENTRY>
		;;<GTKSPINBUTTON>
		<GTKDRAWINGAREA>
		<GTKCURVE>
		<GTKCONTAINER>
		<GTKTREEVIEW>
		<GTKTOOLBAR>
		<GTKTEXTVIEW>
		<GTKTABLE>
		<GTKSOCKET>
		<GTKPANED>
		<GTKVPANED>
		<GTKHPANED>
		<GTKNOTEBOOK>
		<GTKMENUSHELL>
		<GTKMENU>
		<GTKMENUBAR>
		<GTKLIST>
		<GTKLAYOUT>
		<GTKFIXED>
		<GTKBIN>
		<GTKVIEWPORT>
		<GTKSCROLLEDWINDOW>
		<GTKITEM>
		<GTKMENUITEM>
		<GTKTEAROFFMENUITEM>
		<GTKSEPARATORMENUITEM>
		<GTKCHECKMENUITEM>
		<GTKRADIOMENUITEM>
		<GTKIMAGEMENUITEM>
		<GTKLISTITEM>
		<GTKHANDLEBOX>
		<GTKFRAME>
		<GTKASPECTFRAME>
		<GTKEVENTBOX>
		<GTKALIGNMENT>
		<GTKBUTTON>
		<GTKTOGGLEBUTTON>
		<GTKCHECKBUTTON>
		<GTKRADIOBUTTON>
		<GTKOPTIONMENU>
		<GTKBOX>
		<GTKVBOX>
		<GTKCOLORSELECTION>
		<GTKFONTSELECTION>
		<GTKGAMMACURVE>
		<GTKHBOX>
		<GTKSTATUSBAR>
		<GTKCOMBO>
		<GTKBUTTONBOX>
		<GTKVBUTTONBOX>
		<GTKHBUTTONBOX>
		<GTKCLIST>
		<GTKCTREE>
		<GTKCALENDAR>
		<GTKWINDOW>
		<GTKPLUG>
		<GTKDIALOG>
		<GTKMESSAGEDIALOG>
		<GTKINPUTDIALOG>
		<GTKFONTSELECTIONDIALOG>
		<GTKFILESELECTION>
		<GTKCOLORSELECTIONDIALOG>
		<GTKWINDOWGROUP>
)
