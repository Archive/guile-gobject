(use-modules (gnome gobject)
	     (gnome gtk)
	     (oop goops))

(define (app)
  (let* ((window (make <GTKWINDOW> #:type 'toplevel))
	 (hbox   (make <GTKHBOX>))
	 (vbox (make <GTKVBOX>))
	 (button (make <GTKBUTTON> #:label "Hello World!"))
	 (qbutton (make <GTKBUTTON> #:label "Quit"))
	 (label (make <GTKLABEL> #:label "Hello World!")))
    
    (gtk-container-add window hbox)


    (gtk-box-pack-start-defaults vbox button)
    (gtk-box-pack-start-defaults vbox qbutton)
    (gtk-box-pack-start-defaults hbox vbox)
    (gtk-box-pack-start-defaults hbox label)
    
    (gobject-signal-connect button 'clicked (lambda (button) (display "hola mundo")))
    (gobject-signal-connect qbutton 'clicked (lambda (button) (gtk-main-quit)))
    
    (gobject-signal-emit button 'clicked)
    
    (gtk-widget-show-all window)
    (gtk-main)))

(app)


